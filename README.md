<h1>Sending midi commands with Arduino UNO</h1>
<p>
In this example I demonstrate how it is possible to send midi commands through the Arduino UNO.<br>
With each loop, the 4x4 matrix is scanned, identifying the button that was pressed, then 3 bytes are sent by the "MidiMessage()" function.<br>
The commands are sent via the serial port and interpreted, in my case, by the ttymidi program (CLI) and subsequently passed to the amsynth synthesizer through QjackCtl.
<p>
<h3>Command line</h3>
<p>
ttymidi -s /dev/ttyUSB0 -v<br>
<p>
*by default ttymidi use 115200 baudrate.

