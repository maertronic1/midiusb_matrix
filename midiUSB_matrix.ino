int pinLin[]  = {5, 4, 3, 2};
int pinCol[] = {6, 7, 8, 9};
int keys[4][4] = {{1, 2, 3, 4},{5, 6, 7, 8},{9, 10, 11, 12},{13, 14, 15, 16}};

int key = 0; 
int oct = 0;

void MidiMessage(byte b1, byte b2, byte b3)
{
  Serial.write(b1);
  Serial.write(b2);
  Serial.write(b3);
}

void setup()
{
  for (int nL = 0; nL <= 3; nL++) 
  {
     pinMode(pinLin[nL], OUTPUT);
     digitalWrite(pinLin[nL], HIGH);
  }

  for (int nC = 0; nC <= 3; nC++) 
  {
     pinMode(pinCol[nC], INPUT_PULLUP);
  } 
   
  Serial.begin(115200);
}
 
void loop()
{
    //scans all lines, turning off one at a time
    for (int nL = 0; nL <= 3; nL++)
    {
      digitalWrite(pinLin[nL], LOW);
      
      //faz varredura em todas as colunas verificando se tem algum botao apertado
      for (int nC = 0; nC <= 3; nC++) 
      {
        if (digitalRead(pinCol[nC]) == LOW)
        {
          key = keys[nL][nC];
          switch (key)
          {
            case 1:
            MidiMessage(144, 60 + (oct), 127);
            break;
            case 2:
            MidiMessage(144, 61 + (oct), 127);
            break;
            case 3:
            MidiMessage(144, 62 + (oct), 127);
            break;
            case 4:
            MidiMessage(144, 63 + (oct), 127);
            break;
            case 5:
            MidiMessage(144, 64 + (oct), 127);
            break;
            case 6:
            MidiMessage(144, 65 + (oct), 127);
            break;
            case 7:
            MidiMessage(144, 66 + (oct), 127);
            break;
            case 8:
            MidiMessage(144, 67 + (oct), 127);
            break;
            case 9:
            MidiMessage(144, 68 + (oct), 127);
            break;
            case 10:
            MidiMessage(144, 69 + (oct), 127);
            break;
            case 11:
            MidiMessage(144, 70 + (oct), 127);
            break;
            case 12:
            MidiMessage(144, 71 + (oct), 127);
            break;
            case 13:
              if (oct <= 24)
              {
                oct = oct + 12;
              }
            break;
            case 16:
              if (oct >= -36)
              {
                oct = oct - 12;
              }
            break;
          }
          while(digitalRead(pinCol[nC]) == LOW){}
        }        
      }

      digitalWrite(pinLin[nL], HIGH);
      switch (key)
            {
              case 1:
              MidiMessage(144, 60 + (oct), 0);
              break;
              case 2:
              MidiMessage(144, 61 + (oct), 0);
              break;
              case 3:
              MidiMessage(144, 62 + (oct), 0);
              break;
              case 4:
              MidiMessage(144, 63 + (oct), 0);
              break;
              case 5:
              MidiMessage(144, 64 + (oct), 0);
              break;
              case 6:
              MidiMessage(144, 65 + (oct), 0);
              break;
              case 7:
              MidiMessage(144, 66 + (oct), 0);
              break;
              case 8:
              MidiMessage(144, 67 + (oct), 0);
              break;
              case 9:
              MidiMessage(144, 68 + (oct), 0);
              break;
              case 10:
              MidiMessage(144, 69 + (oct), 0);
              break;
              case 11:
              MidiMessage(144, 70 + (oct), 0);
              break;
              case 12:
              MidiMessage(144, 71 + (oct), 0);
              break;                           
            }
      key = 0;
    }
}
 
